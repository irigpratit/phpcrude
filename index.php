<?php
/**
 * Created by PhpStorm.
 * User: irigpratit
 * Date: 1/10/19
 * Time: 10:02 PM
 */

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <title>SMS</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="includes/css/style.css">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">

</head>


<body>
<?php
include ('includes/header.php')
?>

<div class="wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="page-header clearfix">
                    <h2 class="pull-left">Department Details</h2>
                    <a href="views/create.php" class="btn btn-success pull-right">Add New Department</a>
                </div>

                <?php
                include "includes/connection.php";


                // Attempt select query execution
                $sql = "SELECT * FROM department";

                $result = $conn->query($sql);

                if ($result->num_rows > 0) {
                    // output data of each row
                    while($row = $result->fetch_assoc()) {
                        echo "<table class='table'>";
                        echo "<thead>";
                        echo "<tr>";
                        echo "<th>Deptartment ID</th>";
                        echo "<th>Name</th>";
                        echo "<th>Adress</th>";
                        echo "<th>Head</th>";
                        echo "</tr>";
                        echo "</thead>";
                        echo "<tbody>";
                        while($row = mysqli_fetch_array($result)){
                            echo "<tr>";
                            echo "<td>" . $row['dept_id'] . "</td>";
                            echo "<td>" . $row['dept_name'] . "</td>";
                            echo "<td>" . $row['dept_adress'] . "</td>";
                            echo "<td>" . $row['dept_head'] . "</td>";
                            echo "<td>";
                            echo "<a href='views/read.php?id=". $row['dept_id'] ." 'title='Update Record' data-toggle='tooltip'><span class='glyphicon glyphicon-pencil'></span></a>";
                            echo "<a href='views/delete.php?id=". $row['dept_id'] ." 'title='Delete Record' data-toggle='tooltip'><span class='glyphicon glyphicon-trash'></span></a>";
                            echo "</td>";
                            echo "</tr>";
                        }
                        echo "</tbody>";
                        echo "</table>";

                        mysqli_free_result($result);
                    }
                }
                else {
                    echo "0 results";
                }
                $conn->close();
                ?>
            </div>
        </div>
    </div>
</div>
<?php
include "includes/footer.php";
?>
</body>
</html>



