<?php
/**
 * Created by PhpStorm.
 * User: irigpratit
 * Date: 1/10/19
 * Time: 10:04 PM
 */
include "../includes/header.php";
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>View Record</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <link rel="stylesheet" href="../includes/css/style.css">

</head>
<body>
<div class="wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="page-header">
                    <h1>Delete Record</h1>
                </div>
                <form action="../index.php" method="post">
                    <div class="alert alert-danger">
                        <input type="hidden" name="id" value="<?php echo trim($_GET["id"]); ?>"/>
                        <p>Are you sure you want to delete this record?</p><br>
                        <p>
                            <input type="submit" value="Yes" class="btn btn-danger">
                            <?php
                            include "../includes/connection.php";
                            ?>
                            <a href="../index.php" class="btn btn-default">No</a>
                        </p>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>

<?php

$dept_id= $_GET['id'];
$sql="DELETE FROM department WHERE dept_id=$dept_id";

if ($conn->query($sql) === TRUE) {
    echo "Deleted successfully";
}
else {
    echo "Error: " . $sql . "<br>" . $conn->error;
}
$conn->close();

include "../includes/footer.php";


?>
</body>
</html>

