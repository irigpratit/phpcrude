<?php
/**
 * Created by PhpStorm.
 * User: irigpratit
 * Date: 1/10/19
 * Time: 10:04 PM
 */



include "../includes/connection.php";
include "../includes/footer.php";
//include "../includes/header.php";


$dept_id= $_GET['id'];

$sql="SELECT * FROM department WHERE dept_id = $dept_id";

$result=$conn->query($sql);

$row=mysqli_fetch_array($result,MYSQLI_ASSOC);
$dept_id=$row['dept_id'];
$dept_name=$row['dept_name'];
$dept_adress=$row['dept_adress'];
$dept_head=$row['dept_head'];
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>View Record</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <link rel="stylesheet" href="../includes/css/style.css">

</head>
<body>
<div class="wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="page-header">
                    <h2>Edit Record</h2>
                </div>
                <p>Please fill this form and submit to edit department record of the database.</p>
                <form action="read.php" method="post">
                    <div class="form-group <?php echo (!empty($id_err)) ? 'has-error' : ''; ?>">
                        <label>Department ID</label><br>
                        <input type="text" class="form-control form-control-sm" name="dept_id" value="<?php echo $dept_id?>"/>
                        <span class="help-block"><?php echo $id_err;?></span>
                    </div>
                    <div class="form-group <?php echo (!empty($name_err)) ? 'has-error' : ''; ?>">
                        <label>Department Name</label><br>
                        <input type="text" class="form-control form-control-sm" name="dept_name" value="<?php echo $dept_name?>" />
                        <span class="help-block"><?php echo $name_err;?></span>
                    </div>
                    <div class="form-group <?php echo (!empty($adress_err)) ? 'has-error' : ''; ?>">
                        <label>Department Address</label><br>
                        <input type="text" class="form-control form-control-sm" name="dept_adress" value="<?php echo $dept_adress?>" />
                        <span class="help-block"><?php echo $adress_err;?></span>
                    </div>
                    <div class="form-group <?php echo (!empty($head_err)) ? 'has-error' : ''; ?>">
                        <label>Head of Department</label><br>
                        <input type="text" class="form-control form-control-sm" name="dept_head"  value="<?php echo $dept_head?>"/>
                        <span class="help-block"><?php echo $head_err;?></span>
                    </div>
                    <input type="hidden" name="dept_id" value="<?php echo $dept_id; ?>"/>
                    <input type="submit" class="btn btn-primary" value="Submit">
                    <a href="../index.php" class="btn btn-default">Cancel</a>
                </form>
            </div>
        </div>
    </div>
</div>


<?php

include "../includes/connection.php";
$dept_id=$dept_name=$dept_adress=$dept_head="";

$id_err=$name_err=$adress_err=$head_err="";



if($_SERVER["REQUEST_METHOD"] == "POST"){

    // validation of id
    $input_dept_id = trim($_POST["dept_id"]);
    if(empty($input_dept_id)){
        $id_err = "Please enter an id.";
    }
    else{
        $dept_id = $input_dept_id;
    }

    //validation of department name
    $input_dept_name = trim($_POST["dept_name"]);
    if(empty($input_dept_name)){
        $name_err = "Please enter an name.";
    }
    else{
        $dept_name = $input_dept_name;
    }

    //validation of department address
    $input_dept_adress = trim($_POST["dept_adress"]);
    if(empty($input_dept_adress)){
        $adress_err = "Please enter an address.";
    }
    else{
        $dept_adress = $input_dept_adress;
    }

    //validation of department head
    $input_dept_head = trim($_POST["dept_head"]);
    if(empty($input_dept_head)){
        $head_err = "Please enter name of Department head.";
    }
    else{
        $dept_head = $input_dept_head;
    }

    if(empty($id_err) && empty($name_err) && empty($adress_err) && empty($head_err)){
        if($_SERVER["REQUEST_METHOD"]==["POST"]){
            $dept_id = filter_input(INPUT_POST,'dept_id');
            $dept_name = filter_input(INPUT_POST,'dept_name');
            $dept_adress = filter_input(INPUT_POST,'dept_adress');
            $dept_head = filter_input(INPUT_POST,'dept_head');
        }

        $sql = "UPDATE department SET dept_name='$dept_name',dept_adress='$dept_adress', dept_head='$dept_head' WHERE dept_id=$dept_id";

        if ($conn->query($sql) === TRUE) {
            echo "Record updated successfully";

            header("location: ../index.php");

        }
        else {
            echo "Error: " . $sql . "<br>" . $conn->error;
        }
    }
}


$conn->close();

?>

</body>
</html>